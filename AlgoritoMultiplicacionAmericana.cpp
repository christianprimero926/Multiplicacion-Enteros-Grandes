#include<iostream>
#include<stdlib.h>
#include<iostream>
#include <stdio.h>
#include <math.h>
using namespace std;

int main(){
    /**Primer Termino de la Multiplicacion o Multiplicando**/
    int multiplicando[100000]={0};
    /**Segundo Termino de la Multiplicacion o Multiplicador**/
    int multiplicador[100000]={0};
    /**Resusltado de la Multiplicacion Termino a Termino
    (de la suma de sus acarreos y vector final donde se mostrara el resultado) o Producto**/
    int producto[100000]={0};

    /**VARIABLES QUE SE UTILIZARAN**/
    /**La variable "M" guardara el tama�o que tendra el primer entero**/
    /**La variable "m" guardara el tama�o que tendra el segundo entero**/
    /**La variable "k" en el primer ciclo sera para mover la sangria es decir
    al momento de terminar la multiplicacion del primer termino del multiplicador
    con cada termino del multiplicando se movera un espacio(es decir la sangria)
    en el vector y seguira el segundo termino del multiplicador con cada termino
    del multiplicando acumulando el resultado de este un espacio despues de la
    anterior multiplicacion, ya que al sumar el primer valor de la multiplicacion no se toca
    y asi sucesivamente;en el segundo ciclo lo que hace es incrementar su valor uno a uno para
    poder ir llenando el vector con el resultado que nos da, teniendo en cuenta que en el primer
    ciclo toma un valor y al entrar al segundo ese valor se incrementa en 1.**/
    /**La variable "multiplicacion" es donde se almacena temporalmente el valor de la multiplicacion
    entre terminos.**/
    /**La variable "parteEntera" es donde se almacena temporalmente la division entre "multiplicacion" y 10
     para determinar cual es la parte entera de la "multiplicacion".**/
    /**La variable "parteDecimal" es donde se almacena temporalmente el modulo entre "multiplicacion" y 10
     para determinar cual es la parte decimal de "multiplicacion".**/
    /**La variable "suma" es donde se almacena temporalmente la suma entre, lo que hay en el vector
    (que inicialmente esta inicializado en 0),es decir "producto[]", el acarreo de una multiplicacion anterior,
    es decir, "acarreoMult", la parte decimal de la multiplicacion, es decir, "parteDecimal",
     y el acarreo de una suma (entre la parte decimal y el acarreo de una multiplicacion anterior), es decir,
     "acarreoSuma".**/
    /**La variable "partEnteraFinal" es donde se almacena temporalmente la division de "suma" y 10 para determinar cual
    es la parte entera de "suma".**/
    /**La variable "partDecimalFinal" es donde se almacena temporalmente el modulo de "suma" y 10 para determinar cual
    es la parte decimal de "suma".**/
    int M,m,k,multiplicacion,parteEntera,parteDecimal,suma,partEnteraFinal,partDecimalFinal = 0;
    cout<<endl<<endl;
    cout<<"\t\t--------ALGORITMO DE MULTIPLICACION AMERICANA--------"<<endl<<endl<<endl;
    /**Se digita por consola cuantos espacios de nuestro vector inicializado en 0 vamos a utilizar**/
    cout<<"Introduce el Tama�o que tendra el primer termino de la Multiplicacion"<<endl;
    /**Cantidad de digitos que va a tener nuestro entero**/
    cin>>M;
    cout<<"Ahora introduce el valor del primer termino"<<endl;
    cout<<"(OJO :Termino a Termino, es decir despues de ingresar un digito"<<endl;
    cout<<"se debe presionar Enter e ingresa el sgte, hasta llegar al tama�o"<<endl;
    cout<<"maximo del que hemos digitado anteriormennte )"<<endl;
    /**Ciclo con el cual se ira llenando espacio a espacio nuestro vector para nuestro primer termino**/
    for(int i=0; i < M; i++){
        cin>>multiplicando[i];
    }
    /**Valor que ingreso mostrado en pantalla**/
    cout<<"Primer Valor Ingresado ---->";
    for(int i=0; i < M; i++){cout<<"|"<<multiplicando[i];}
    cout<<"|"<<endl<<endl;

    /**Se digita por consola cuantos espacios de nuestro vector inicializado en 0 vamos a utilizar**/
    cout<<"Introduce el Tama�o que tendra el segundo termino de la Multiplicacion"<<endl;
     /**Cantidad de digitos que va a tener nuestro entero**/
    cin>>m;
    cout<<"Ahora introduce el valor del segundo termino"<<endl;
    cout<<"(OJO :Termino a Termino, se aplica lo mismo que lo anterior)"<<endl;
    /**Ciclo con el cual se ira llenando espacio a espacio nuestro vector para nuestro segundo termino**/
    for(int i=0; i < m; i++){
        cin>>multiplicador[i];
    }
    /**Valor que ingreso mostrado en pantalla**/
    cout<<"Segundo Valor Ingresado ---->";
    for(int i=0; i < m; i++){cout<<"|"<<multiplicador[i];}
    cout<<"|"<<endl<<endl<<endl;

    /**A continuacion sigue el ciclo con el cual se va hacer la multiplicacion termino a termino
    hasta no tener mas terminos con que multiplicarse y nos mostrara un vector con el resultado
    se Tiene en cuenta que la multiplicacion tiene una complejidad de O(n^2) asi que por intuicion
    se puede decir que se manejaran 2 ciclos para manejar esta funcion **/

    cout<<"\t\t-----PASO A PASO DE LA MULTIPLICACION-----"<<endl<<endl;
    for(int i=0; i <100; i++){cout<<"--";}
    cout<<endl;

    for(int i=m, a=0; i>=0; i--, a++){
        k=a;
        int acarreoMult =0;
        int acarreoSuma =0;
        cout<<"Multiplicacion del termino #"<<a+1<<" * el Multiplicando"<<endl<<endl;
        for(int j=M; j>=0; j--, k++){
            /****/
            multiplicacion = multiplicador[i-1]*multiplicando[j-1];
            parteEntera = multiplicacion/10;
            parteDecimal = multiplicacion%10;

            suma = producto[k]+acarreoMult+parteDecimal+acarreoSuma;
            partEnteraFinal = suma/10;
            partDecimalFinal = suma%10;

            producto[k] = partDecimalFinal;
            acarreoMult = parteEntera;
            acarreoSuma = partEnteraFinal;

            /**NOTA: SI SE DESEA SE PUEDE COMENTAR LOS SIGUIENTES LLAMADOS A PANTALLA, SON SOLO PARA VER COMO
            TRABAJA EL ALGORITMO**/

            cout<<"Se multiplica "<<multiplicador[i-1]<<" * "<<multiplicando[j-1]<<" = "<<multiplicacion<<endl;
            cout<<"Se toma la parte entera, que seria : "<<parteEntera<<endl;
            cout<<"Se toma la parte decimal, que seria : "<<parteDecimal<<endl;
            cout<<"Ahora se suma lo que habia antes en el vector : "<<producto[k]<<endl;
            cout<<"el acarreo de la multiplicacion anterior : "<<acarreoMult<<endl;
            cout<<"la parte decimal de la multiplicacion que se acaba de hacer : "<<parteDecimal<<endl;
            cout<<"y el acarreo de esta misma suma(pero la anterior) : "<<acarreoSuma<<endl;
            cout<<"y quedaria de esta forma : "<<producto[k]<<" + "<<acarreoMult<<" + "<<parteDecimal<<" + "<<acarreoSuma<<" = "<<suma<<endl;
            cout<<"Ahora se toma la parte entera de esta suma, que seria : "<<partEnteraFinal<<endl;
            cout<<"y se toma la parte decimal de la suma, que seria : "<<partDecimalFinal<<endl<<endl;
            cout<<"El valor final que se ingresara en el vector sera : "<<producto[k]<<endl;
            cout<<"El acarreo para la sgte Multiplicacion va a ser : "<<acarreoMult<<endl;
            cout<<"Y el acarreo para la sgte suma va a ser : "<<acarreoSuma<<endl;
            cout<<"Y este es el vector que queda luego de realizar estos pasos-->"<<endl;
            for (int j = ((M+m)-1); j >= 0 ; j--){cout<<" | "<<producto[j];}
            cout<<endl;
        }
        cout<<endl;
        for(int i=0; i <100; i++){cout<<"--";}
        cout<<endl;
    }
    cout<<endl;
    for(int i=0; i <100; i++){cout<<"--";}
    cout<<"\t\t-----PASO A PASO DE LA MULTIPLICACION-----"<<endl<<endl;
    system("PAUSE");
    system("cls");

    for(int i=0; i <100; i++){cout<<"--";}cout<<endl;
    cout<<"\t\t----------ENTEROS A MULTIPLICAR----------"<<endl<<endl;
    for(int i=0; i < M; i++){cout<<" | "<<multiplicando[i];}
    cout<<" | *";
    for(int i=0; i < m; i++){cout<<" | "<<multiplicador[i];}
    cout<<" |"<<endl;
    cout<<"\t\t----------RESULTADO FINAL----------"<<endl<<endl;
    for (int j = ((M+m)-1); j >= 0 ; j--){cout<<" | "<<producto[j];}
    cout<<" |"<<endl<<endl;
    cout<<"\t\t----------RESULTADO FINAL----------"<<endl<<endl;
    system("PAUSE");

    return 0;
}
