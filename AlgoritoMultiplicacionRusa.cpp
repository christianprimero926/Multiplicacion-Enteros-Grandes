#include<iostream>
#include<stdlib.h>
#include<iostream>
#include <stdio.h>
#include <math.h>
using namespace std;


void algoritmoRuso(int multiplicando[],int multiplicador[],int producto[],int cociente[],int resultadoFinal[], int M,int m){
    /**VARIABLES QUE SE UTILIZARAN**/
    /**La variable "division" es donde se almacena temporalmente el valor de la division
    entre 2.**/
    /**La variable "resto" es donde se almacena temporalmente el modulo 2 de "division"
    y se multiplica por 10 para determinar cual es el acarreo.**/
    /**La variable "siguiente" es donde se almacena temporalmente la suma entre,
    el acarreo de la division anterior, es decir, "resto" y el siguiente termino del multiplicando.**/

    /**La variable "multiplicacion" es donde se almacena temporalmente el valor de la multiplicacion
    entre 2.**/
    /**La variable "parteEntera" es donde se almacena temporalmente la division entre "multiplicacion" y 10
     para determinar cual es la parte entera de la "multiplicacion".**/
    /**La variable "parteDecimal" es donde se almacena temporalmente el modulo entre "multiplicacion" y 10
     para determinar cual es la parte decimal de "multiplicacion".**/
    /**La variable "suma" es donde se almacena temporalmente la suma entre,  el acarreo de la multiplicacion anterior,
    es decir, "acarreoMult" y la parte decimal de la multiplicacion, es decir, "parteDecimal".**/
    /**La variable "acarreoMult" se almacena la parte entera de la multiplicacion, que vendra siendo el acarreo de esta **/

    /**La variable "sumaFinal" es donde se almacena temporalmente el valor de la suma entre terminos
    del vector "resultadoFinal[]" y multiplicador[].**/
    /**La variable "parteEnteraSuma" es donde se almacena temporalmente la division de "sumaFinal" y 10 para determinar cual
    es la parte entera de "sumaFinal".**/
    /**La variable "parteDecimalSuma" es donde se almacena temporalmente el modulo de "sumaFinal" y 10 para determinar cual
    es la parte decimal de "sumaFinal".**/
    /**La variable "total" es donde se almacena temporalmente la suma entre,  el acarreo de la suma anterior,
    es decir, "acarreoSuma" y la parte decimal de la suma, es decir, "parteDecimalSuma".**/
    /**La variable "parteEnteraFinal" es donde se almacena temporalmente la division de "total" y 10 para determinar cual
    es la parte entera de "total".**/
    /**La variable "parteDecimalFinal" es donde se almacena temporalmente el modulo de "total" y 10 para determinar cual
    es la parte decimal de "total".**/
    /**La variable "acarreoSuma" se almacena la parte entera de la suma, que vendra siendo el acarreo de esta **/

    int division,resto,siguiente = 0;
    int sumaFinal,parteEnteraSuma,parteDecimalSuma,total,parteEnteraFinal,parteDecimalFinal,acarreosuma = 0;
    int multiplicacion,parteEntera,parteDecimal,suma,acarreoMult =0;

    for(int j=0,k=0; j<M; j++,k++){

            division = multiplicando[j]/2;
            resto = (multiplicando[j]%2)*10;

            siguiente = resto + multiplicando[j+1];

            cociente[k] = division;
            multiplicando[j+1]=siguiente;
//            cout<<"Se divide "<<multiplicando[j]<<" /2 = "<<division<<endl;
//            cout<<"Se toma el resto, que seria : "<<resto<<endl;
//            cout<<"Ahora se suma = "<<resto<<" + "<<multiplicando[j+1]<<" = "<<siguiente<<endl;
    }
    int k=0;
    if(multiplicador[0]==0){
        for(int i=0;i<=m;i++){
            multiplicador[i]=multiplicador[i+1];
        }
    }
    if(multiplicando[M-1]%2!=0){
            if(resultadoFinal[m-1]==0 && multiplicador[m-1]!=0){
                for(int i=m;i>=0;i--){
                    resultadoFinal[i]=resultadoFinal[i-1];
                }
            }
            for(int i=m,h=m;i>=0;i--,h--){
                sumaFinal = resultadoFinal[i]+multiplicador[i];
                parteEnteraSuma = sumaFinal/10;
                parteDecimalSuma = sumaFinal%10;
//                    cout<<"Se suma resultadoFinal en la posicion "<<i<<" "<<resultadoFinal[i]<<endl;
//                    cout<<"Se suma multiplicador en la posicion "<<i<<" "<<multiplicador[i]<<endl;
//                    cout<<"Se suma resultado "<<resultadoFinal[i]<<" + "<<multiplicador[i]<<" = "<<sumaFinal<<endl<<endl;

                total = acarreosuma+parteDecimalSuma;
                parteEnteraFinal = total/10;
                parteDecimalFinal = total%10;
//                    cout<<"Ahora se suma = "<<acarreosuma<<" + "<<parteDecimalSuma<<" = "<<total<<endl;

                resultadoFinal[i] = parteDecimalFinal;
                acarreosuma = parteEnteraFinal+parteEnteraSuma;

//                    cout<<"Se toma la parte entera, que seria : "<<parteEnteraSuma<<endl;
//                    cout<<"Se toma la parte decimal, que seria : "<<parteDecimalSuma<<endl;
//                    cout<<"El valor final que se ingresara en el vector sera : "<<resultadoFinal[i]<<endl;
//                    cout<<"El acarreo para la sgte suma va a ser : "<<acarreosuma<<endl<<endl;

                if( h==0 && acarreosuma!=0){
                    resultadoFinal[h-1] = acarreosuma;
//                        cout<<"El valor final que se ingresara en el vector sera : "<<resultadoFinal[h]<<endl;
                }
//                    for(int i=0;i<=m;i++){
//                        cout<<"|"<<resultadoFinal[i]<<"|";
//                    }cout<<endl<<endl;
            }
    }
    for(int i=m-1; i>=0; i--){

            multiplicacion = multiplicador[i]*2;
            parteEntera = multiplicacion/10;
            parteDecimal = multiplicacion%10;

            suma = acarreoMult+parteDecimal;

            producto[k] = suma;
            acarreoMult = parteEntera;
//            cout<<"Se multiplica "<<multiplicador[i]<<" * 2 = "<<multiplicacion<<endl;
//            cout<<"Se toma la parte entera, que seria : "<<parteEntera<<endl;
//            cout<<"Se toma la parte decimal, que seria : "<<parteDecimal<<endl;
//            cout<<"Ahora se suma = "<<acarreoMult<<" + "<<parteDecimal<<" = "<<suma<<endl;
//            cout<<"El valor final que se ingresara en el vector sera : "<<producto[k]<<endl;
//            cout<<"El acarreo para la sgte Multiplicacion va a ser : "<<acarreoMult<<endl;
            k++;
//            cout<<"cuanto vale s = "<<s<<endl;
//            cout<<"cuanto vale k = "<<k<<endl;
            if( k==m && acarreoMult!=0){
                producto[k] = acarreoMult;
            }
    }
}
void igualAUno(int multiplicando[],int multiplicador[],int producto[],int cociente[],int residuo[],int resultadoFinal[],int M,int m){
    int comparacion = 0;
    for(int i = 0; i < M;i++){
             if(multiplicando[i] == residuo[i]){
                comparacion++;
             }
             if(comparacion == M){
                 cout<<""<<endl;
             }
    }
    if(comparacion < M){
            algoritmoRuso(multiplicando,multiplicador,producto,cociente,resultadoFinal,M,m);
            cout<<endl;
            for(int i=0;i<M;i++){
                    multiplicando[i]=cociente[i];
                }
            for(int i=0;i<M;i++){
                cout<<"|"<<multiplicando[i]<<"|";
            }
            int s = 0;
            int j = 0;
            if(producto[m]!=0){
                    s = m+1;
                    j = m;
                    for(int i=0; i<=m, j>=0; i++, j--){
                            multiplicador[i]=producto[j];
                        }
                        m = s;

            }else{
                for(int i=0, j=m; i<=m, j>=0; i++, j--){
                        multiplicador[i]=producto[j];
                }
            }
            cout<<"\t*\t";
            for(int i=0;i<=m;i++){
                cout<<"|"<<multiplicador[i]<<"|";
            }
            cout<<endl;
            igualAUno(multiplicando,multiplicador,producto,cociente,residuo,resultadoFinal,M,m);
        }
}

int main(){
    /**Primer Termino de la Multiplicacion o Multiplicando**/
    int multiplicando[10000]={0};
    /**Segundo Termino de la Multiplicacion o Multiplicador**/
    int multiplicador[10000]={0};
    /**Resultado de la Multiplicacion por 2**/
    int producto[10000]={0};
    /**Resultado de la Division por 2**/
    int cociente[10000]={0};
    /**Vector para comparar**/
    int residuo[10000]={0};

    int resultadoFinal[10000]={0};

    /**VARIABLES QUE SE UTILIZARAN**/
    /**La variable "M" guardara el tama�o que tendra el primer entero**/
    /**La variable "m" guardara el tama�o que tendra el segundo entero**/
    int M,m = 0;
    cout<<endl<<endl;
    cout<<"\t\t--------ALGORITMO DE MULTIPLICACION RUSA--------"<<endl<<endl<<endl;

    /**Se digita por consola cuantos espacios de nuestro vector inicializado en 0 vamos a utilizar**/
    cout<<"Introduce el Tama�o que tendra el primer termino de la Multiplicacion"<<endl;
    /**Cantidad de digitos que va a tener nuestro entero**/
    cin>>M;
    cout<<"Ahora introduce el valor del primer termino"<<endl;
    cout<<"(OJO :Termino a Termino, es decir despues de ingresar un digito"<<endl;
    cout<<"se debe presionar Enter e ingresa el sgte, hasta llegar al tama�o"<<endl;
    cout<<"maximo del que hemos digitado anteriormennte )"<<endl;
    /**Ciclo con el cual se ira llenando espacio a espacio nuestro vector para nuestro primer termino**/
    //int multiplicando[M-1];
    for(int i=0; i < M; i++){
        cin>>multiplicando[i];
    }
    /**Valor que ingreso mostrado en pantalla**/
    cout<<"Primer Valor Ingresado ---->";
    for(int i=0; i < M; i++){cout<<"|"<<multiplicando[i];}
    cout<<"|"<<endl<<endl;

    /**Se digita por consola cuantos espacios de nuestro vector inicializado en 0 vamos a utilizar**/
    cout<<"Introduce el Tama�o que tendra el segundo termino de la Multiplicacion"<<endl;
     /**Cantidad de digitos que va a tener nuestro entero**/
    cin>>m;
    cout<<"Ahora introduce el valor del segundo termino"<<endl;
    cout<<"(OJO :Termino a Termino, se aplica lo mismo que lo anterior)"<<endl;
    /**Ciclo con el cual se ira llenando espacio a espacio nuestro vector para nuestro segundo termino**/

    for(int i=0; i < m; i++){
        cin>>multiplicador[i];
    }
    /**Valor que ingreso mostrado en pantalla**/
    cout<<"Segundo Valor Ingresado ---->";
    for(int i=0; i < m; i++){cout<<"|"<<multiplicador[i];}
    cout<<"|"<<endl<<endl<<endl;

    for(int i=0; i < M; i++){
        if((M-1)!=i){
            residuo[i]=0;
        }else{
            residuo[i]=0;
        }
        //cout<<"|"<<residuo[i]<<"|";
    }
    //cout<<" |"<<endl<<endl;
    cout<<"\t\t-----PASO A PASO DE LA MULTIPLICACION-----"<<endl<<endl;

    for(int i=0; i < M; i++){cout<<"|"<<multiplicando[i];}
    cout<<"|";
    cout<<"\t*\t";
    for(int i=0; i < m; i++){cout<<"|"<<multiplicador[i];}
    cout<<"|"<<endl;
    for(int i=0; i <100; i++){cout<<"--";}cout<<endl;
    igualAUno(multiplicando,multiplicador,producto,cociente,residuo,resultadoFinal,M,m);
    for(int i=0; i <100; i++){cout<<"--";}cout<<endl<<endl;
    cout<<"\t\t----------RESULTADO FINAL----------"<<endl<<endl;

    cout<<"\tvalor vector final resultado = ";
            for(int i=0;i<(m+M);i++){
                cout<<"|"<<resultadoFinal[i]<<"|";
            }cout<<endl<<endl;
    cout<<"\t\t----------RESULTADO FINAL----------"<<endl<<endl;
    system("pause");
}
